CP = nasm -g -f elf64
LD = ld
PYTHON = python3

SRC = $(wildcard src/*.asm)
OBJS = $(patsubst %.asm,%.o,$(SRC))

.PHONY: main test clean

main: $(OBJS)
	$(LD) -o $@ $^

test:
	$(PYTHON) test.py

clean:
	rm src/*.o main

%.o: %.asm
	$(CP) -o $@ $< -Isrc
