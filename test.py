#!/usr/bin/python3

import subprocess
import unittest
from subprocess import CalledProcessError, Popen, PIPE

__unittest = True

NOT_FOUND = 'No such key was found.'

def compile():
    assert subprocess.call(['make']) == 0, 'failed to make'

class DictTest(unittest.TestCase):
    def launch(self, input):
        try:
            p = Popen(['./main'], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (stdout, stderr) = p.communicate(input.encode())
            output = stderr if stdout == b'' else stdout
            return (output.decode(), p.returncode)
        except CalledProcessError as e:
            return (exc.output.decode(), exc.returncode)

    def perform(self, input, output):
        (o, c) = self.launch(input)
        self.assertEqual(o, output, f"Provided '{input}', expected '{output}', got '{o}'")

    def test_ok(self):
        cases = (
            ('first word\0', 'first word explanation'),
            ('second word\0', 'second word explanation'),
            ('third word\0', 'third word explanation'),
        )

        for case in cases:
            self.perform(*case)

    def test_not_ok(self):
        cases = (
            ('', NOT_FOUND),
            (' ', NOT_FOUND),
            ('\0', NOT_FOUND),
            ('t', NOT_FOUND),
            ('third', NOT_FOUND),
            ('third \n', NOT_FOUND),
            ('first', NOT_FOUND),
        )

        for case in cases:
            self.perform(*case)

if __name__ == "__main__":
    compile()
    unittest.main(failfast=False, buffer=False, catchbreak=False)
