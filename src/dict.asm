%include "lib.inc"

%define DQ_SIZE 8

global find_word

section .text
find_word:
        ; arguments
        ; rdi -- string pointer
        ; rsi -- dict pointer
        ; variables
        ; r8 -- string pointer
        ; r9 -- dict item pointer
        ; rax -- buffer
        mov     r8, rdi         ; r8 -- string pointer
        mov     r9, rsi         ; r9 -- dict item pointer
.loop:
        test    r9, r9
        je      .list_ended_err ; if list ended

        ; rdi -- string pointer
        mov     rdi, r8
        ; rsi -- dict item pointer
        lea     rsi, [r9 + DQ_SIZE]

        call    string_equals   ; comparing strings
        test    rax, rax
        jnz     .found          ; if equals, we found

        mov     r9, [r9]        ; item = *item
        jmp     .loop           ; checking next item
.list_ended_err:
        xor     r9, r9          ; r9 = 0
.found:
        mov     rax, r9         ; rax -- dict item pointer
        ret
