%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 256

global _start

section .data
buffer: times BUFFER_SIZE db 0
not_found_msg: db "No such key was found.", 0

section .text
_start:
        ; read a buffer via read_char x 255
        mov     rdi, buffer
        mov     rsi, BUFFER_SIZE
        call    read_string     ; reading word from stdin
.find:
        mov     rdi, buffer
        mov     rsi, DICT_PTR
        call    find_word       ; finding a word in dict
        test    rax, rax
        jz      .not_found      ; if not found
.found:                         ; else
        ; rax -- key
        lea     rax, [rax + DQ_SIZE]
        mov     rbx, rax        ; saving key ptr to rbx

        mov     rdi, rbx
        call    string_length   ; finding length of key

        ; ptr to value = ptr to key + key length + \0
        lea     rbx, [rbx + rax + 1]

        mov     rdi, rbx
        call    print_string    ; printing result
        jmp     .end            ; going to end
.not_found:
        mov     rdi, not_found_msg
        call    print_error
	syscall
.end:
	xor	rdi, rdi
        call    exit            ; exiting

print_error:
        push	rdi
	call	string_length
	pop	rdi
	mov	rdx, rax
	mov	rax, 1
	mov	rsi, rdi
	mov	rdi, 2          ; stderr
        syscall
        ret

read_string:
	; arguments
	; rdi -- buffer pointer
	; rsi -- buffer size
	; variables
	; rdi -- buffer pointer
	; rsi -- position pointer
	; rcx -- buffer end pointer
	; rdx -- is word started
	mov	rcx, rdi	;
	add	rcx, rsi	; rcx = rdi + rsi
	dec	rcx		; rcx = rcx - 1 (for \0)
	mov	rsi, rdi	; rsi = rdi
	xor	rdx, rdx	; rdx = false
.loop:
	; reading character
	push	rdi
	push	rsi
	push	rcx
	push	rdx
	call	read_char
	pop	rdx
	pop	rcx
	pop	rsi
	pop	rdi
	; checking for EOF
	cmp	al, 0
	je	.ok		; if EOF
	; checking for whitespace
	cmp	al, 0x0a
	je	.ws_yes		; if char == '\n'
	jmp	.ws_no		; else
.ws_yes:
	; if whitespace
	cmp	rdx, 0
	je	.loop		; if word not started
	jmp	.ok		; else
.ws_no:
	; if not whitespace
	mov	rdx, 1		; rdx = true
	mov	byte [rsi], al	; saving character
	inc	rsi		; setting next position
	cmp	rsi, rcx
	jl	.loop		; if buffer not ended
	jmp	.small_buf_err	; else
.ok:
	mov	byte [rsi], 0	; adding '\0' to the end
	mov	rax, rdi	; rax -- buffer start
	sub	rsi, rdi	; rsi -- word length
	mov	rdx, rsi	; rdx = rsi
	ret
.eof_err:
.small_buf_err:
	xor	rax, rax	; rax = 0
	ret