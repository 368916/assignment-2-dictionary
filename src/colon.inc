%define DICT_PTR 0

%macro colon 2
%2:
dq      DICT_PTR
db      %1, 0

%define DICT_PTR %2
%endmacro
